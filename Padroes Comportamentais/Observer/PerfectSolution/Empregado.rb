require 'observer'

class Empregado
    include Observable

    attr_reader :nome, :salario

    def initializer(nome: nil, salario: nil)
        @nome = nome
        @salario = salario
        @observers = []
    end

    def salario=(novo_salario)
        @salario = novo_salario
        changed
        notify_observers(self)
    end

end
