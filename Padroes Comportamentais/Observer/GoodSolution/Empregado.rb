class Empregado

    attr_reader :nome, :salario

    def initialize(nome:nil, salario: nil, pagamento:nil)
        @nome = nome
        @salario = salario
        @observers = []
    end

    def salario=(novo_salario)
        @salario = novo_salario
        notify_observers
    end

    def add_observer(observer)
        @observers << observer
    end

    def add_observer(observer)
        @observers.delete(observer)
    end

     def notify_observers()
        @observers.each do |observer|
            observer.atualizar(self)
        end    
    end

end
