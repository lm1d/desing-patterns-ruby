# require_relative to import if you needs, example:
# require_relative 'pagamento'
# require_relative 'receita_federal'

someone = Empregado.new(nome:"Someone", salario: 250)

someone.add_observer(Pagamento.new())
someone.add_observer(ReceitaFederal.new())

someone.salario = 500
