class Semaforo

    attr_accessor :estado

    def initializer (estado: Verde.new)
        @estado = estado
    end

    def tick
        @estado = @estado.tick
    end

end
