class Vermelho < Estado
    def tick
        puts "O Sinal está Vermelho"
        return Verde.new
    end
end

class Verde < Estado
    def tick
        puts "O Sinal está Verde"
        return Amarelo.new
    end
end

class Amarelo < Estado
    def tick
        puts "O Sinal está Amarelo"
        return Vermelho.new
    end
end